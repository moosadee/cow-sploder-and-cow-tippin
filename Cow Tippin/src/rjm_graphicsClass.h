#pragma once

class rjm_graphicsClass
{
    private:
        int red_x;
        int red_y;
        int red_w;
        int red_h;
        int red_tx;
        int red_ty;

        int blue_x;
        int blue_y;
        int blue_w;
        int blue_h;
        int blue_tx;
        int blue_ty;

        int anim1;

        int arrow_y;    //Arrow has no X because it's x is whichever player's head it's above
        int arrow_tx;
        int arrow_ty;
        int arrow_w;
        int arrow_h;
        int mouse_red_tx;
        int mouse_red_ty;
        int mouse_blue_tx;
        int mouse_blue_ty;
        int mouse_w;
        int mouse_h;
        int anim2;
    public:
        rjm_graphicsClass();
        void draw( BITMAP *buffer, BITMAP *graphics, int turn, float counter );
        void draw_cursor( BITMAP *buffer, BITMAP *graphics, int mouse_x, int mouse_y, int turn, float counter );
};

rjm_graphicsClass::rjm_graphicsClass()
{
    red_x = 605;    //x coordinate of graphic
    red_y = 350;    //y coordinate of graphic
    red_w = 80;     //width of graphic
    red_h = 160;    //height of graphic
    red_tx = 800;   //the x coordinate of graphic on the tileset
    red_ty = 0;     //the y coordinate of the graphic on the tileset

    blue_x = 700;
    blue_y = 350;
    blue_w = 80;
    blue_h = 160;
    blue_tx = 800;
    blue_ty = 160;

    anim1 = 80;

    arrow_y = 260;
    arrow_tx = 800;
    arrow_ty = 320;
    arrow_w = 80;
    arrow_h = 80;

    mouse_red_tx = 838;
    mouse_red_ty = 512;
    mouse_blue_tx = 854;
    mouse_blue_ty = 512;
    mouse_w = 16;
    mouse_h = 16;
    anim2 = 16;
}

void rjm_graphicsClass::draw( BITMAP *buffer, BITMAP *graphics, int turn, float counter )
{
    //Draw player graphics
    if ( turn == 0 )
    {
        masked_blit( graphics, buffer, red_tx + anim1, red_ty, red_x, red_y, red_w, red_h );    //red player
        masked_blit( graphics, buffer, blue_tx, blue_ty, blue_x, blue_y, blue_w, blue_h );      //blue player
        masked_blit( graphics, buffer, arrow_tx, arrow_ty, red_x, arrow_y + ((int)counter%5), arrow_w, arrow_h );  //arrow
    }
    else
    {
        masked_blit( graphics, buffer, red_tx, red_ty, red_x, red_y, red_w, red_h );
        masked_blit( graphics, buffer, blue_tx + anim1, blue_ty, blue_x, blue_y, blue_w, blue_h );
        masked_blit( graphics, buffer, arrow_tx, arrow_ty, blue_x, arrow_y + ((int)counter%5), arrow_w, arrow_h );  //arrow
    }
}

void rjm_graphicsClass::draw_cursor( BITMAP *buffer, BITMAP *graphics, int mouse_x, int mouse_y, int turn, float counter )
{
    if ( turn == 0 )
    {
        if ( (int)counter % 4 < 2 )     //animated mouse
            masked_blit( graphics, buffer, mouse_red_tx, mouse_red_ty, mouse_x, mouse_y, mouse_w, mouse_h );
        else
            masked_blit( graphics, buffer, mouse_red_tx, mouse_red_ty + anim2, mouse_x, mouse_y, mouse_w, mouse_h );
    }
    else
    {
        if ( (int)counter % 4 < 2 )
            masked_blit( graphics, buffer, mouse_blue_tx, mouse_blue_ty, mouse_x, mouse_y, mouse_w, mouse_h );
        else
            masked_blit( graphics, buffer, mouse_blue_tx, mouse_blue_ty + anim2, mouse_x, mouse_y, mouse_w, mouse_h );
    }

}

